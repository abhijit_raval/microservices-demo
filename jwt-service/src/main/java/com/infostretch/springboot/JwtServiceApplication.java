package com.infostretch.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = { "com.infostretch" })
@EntityScan(basePackages = { "com.infostretch.mdoel" })
@EnableJpaRepositories(basePackages = { "com.infostretch.repository" })
@EnableEurekaClient
@EnableDiscoveryClient
@EnableZuulProxy
public class JwtServiceApplication {
	public static void main(String[] args) {
		SpringApplication.run(JwtServiceApplication.class, args);
	}

}
