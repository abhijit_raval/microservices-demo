package com.infostretch.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.infostretch.feignclient.FeignClient;
import com.infostretch.model.ItemModel;

@RestController
@RequestMapping("v1/feign")
public class FeignController {
	private static final Logger logger = LoggerFactory.getLogger(FeignController.class);
	private FeignClient feignClient;

	@Autowired
	public FeignController(FeignClient feignClient) {
		this.feignClient = feignClient;
	}

	@GetMapping("/itemfeign")
	public List<ItemModel> getItems() throws IOException {
		System.out.println("In getItems");
		logger.info("In feignclient");
		List<ItemModel> itemModels = new ArrayList<ItemModel>();
		itemModels.addAll(feignClient.getItems());
		itemModels = itemModels.stream().map(item -> item).filter(item -> item.getName().startsWith("A"))
				.collect(Collectors.toList());
		return itemModels;
	}
	
	@GetMapping("/feignitems")
	public String items() throws IOException {
		System.out.println("In items");
		List<ItemModel> itemModels=new ArrayList<ItemModel>();
		System.out.println("------------------------"+feignClient.items());
		return feignClient.items();
	}

	@Controller
	public static class FallBack {
		public String greeting() {
			System.out.println("In greeting");
			return "Hello User!";
		}
	}
}
