package com.infostretch.feignclient;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

import com.infostretch.controller.FeignController;
import com.infostretch.model.ItemModel;

@org.springframework.cloud.openfeign.FeignClient(value = "item-catalog-service",
												fallback = FeignController.FallBack.class)
public interface FeignClient {

	@GetMapping("v1/cart/items")
	public List<ItemModel> getItems();
	
	@GetMapping("v1/cart/items")
	public String items();

}
