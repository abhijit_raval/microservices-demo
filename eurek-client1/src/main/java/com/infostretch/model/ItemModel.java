package com.infostretch.model;

public class ItemModel {

		private long id;
		private String name;
		private String manufacturer;
		private String seller;

		public ItemModel() {
			super();
			// TODO Auto-generated constructor stub
		}

		public long getId() {
			return id;
		}

		public void setId(long id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getManufacturer() {
			return manufacturer;
		}

		public void setManufacturer(String manufacturer) {
			this.manufacturer = manufacturer;
		}

		public String getSeller() {
			return seller;
		}

		public void setSeller(String seller) {
			this.seller = seller;
		}

		@Override
		public String toString() {
			return "ItemDTO [id=" + id + ", name=" + name + ", manufacturer=" + manufacturer + ", seller=" + seller + "]";
		}

}

