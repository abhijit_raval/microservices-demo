package com.infostretch.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication(scanBasePackages = {"com.infostretch"})
@EnableFeignClients(basePackages = {"com.infostretch.feignclient","com.infostretch.controller"})
public class EurekClient1Application {

	public static void main(String[] args) {
		SpringApplication.run(EurekClient1Application.class, args);
	}

}
