package com.infostretch.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("v1/order")
public class OrderController {

	private static final Logger logger = LoggerFactory.getLogger(OrderController.class);

	@Autowired
	RestTemplate restTemplate;

	@GetMapping("/itemdetails")
	public String getItem() {
		String response = restTemplate.exchange("http://localhost:8088/v1/cart/items", HttpMethod.GET, null,
				new ParameterizedTypeReference<String>() {
				}).getBody();
		logger.info("Inside zipkinService");
		return response;
	}
}
