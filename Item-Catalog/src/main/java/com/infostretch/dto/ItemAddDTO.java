package com.infostretch.dto;

public class ItemAddDTO {
	
	private String name;
	private String manufacturer;
	private String seller;

	public ItemAddDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getSeller() {
		return seller;
	}

	public void setSeller(String seller) {
		this.seller = seller;
	}

	@Override
	public String toString() {
		return "ItemDTO [name=" + name + ", manufacturer=" + manufacturer + ", seller=" + seller + "]";
	}
}
