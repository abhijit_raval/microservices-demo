package com.infostretch.aspect;

import java.util.concurrent.TimeUnit;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ItemAspect {
	public static final Logger logger = LoggerFactory.getLogger(ItemAspect.class);
	private long startTime;
	@Before("execution(* com.infostretch.services.ItemServiceImpl.*(..))")
	public void beforeMethod(JoinPoint joinpoint) {
		startTime = System.nanoTime();
		logger.info("Starting time of {} method execution:{}", joinpoint.getSignature().getName(), startTime);
	}

	@After("execution(* com.infostretch.services.ItemServiceImpl.*(..))")
	public void afterMethod(JoinPoint joinPoint) {
		long endTime = System.nanoTime();
		long totalTime = TimeUnit.NANOSECONDS.toMillis((endTime - startTime));
		logger.info("Ending time of {} method execution:{} Microseconds", joinPoint.getSignature().getName(), endTime);
		logger.info("TOTAL EXECUTION TIME OF {} is:{}ms", joinPoint.getSignature().getName(), totalTime);
	}
}
