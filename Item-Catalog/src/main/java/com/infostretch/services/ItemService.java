package com.infostretch.services;

import java.util.List;

import com.infostretch.dto.ItemAddDTO;
import com.infostretch.dto.ItemDTO;
import com.infostretch.entity.Item;

public interface ItemService {
	public List<ItemDTO> getItems();
	public void addItems(ItemAddDTO itemAddDTO);
	public void deleteItem(long id);
	public void updateItem(long id,ItemAddDTO itemDTO);
}
