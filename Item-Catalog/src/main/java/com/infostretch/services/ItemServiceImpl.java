package com.infostretch.services;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.infostretch.dto.ItemAddDTO;
import com.infostretch.dto.ItemDTO;
import com.infostretch.entity.Item;
import com.infostretch.repository.ItemRepository;

@Service
public class ItemServiceImpl implements ItemService {

	@Autowired
	ItemRepository itemRepo;
	@Autowired
	ModelMapper mapper;

	@Override
	public List<ItemDTO> getItems() {
		// TODO Auto-generated method stub
		List<ItemDTO> itemDTOs = new ArrayList<ItemDTO>();
		for (Item item : itemRepo.findAll()) {
			ItemDTO tempDTO = mapper.map(item, ItemDTO.class);
			itemDTOs.add(tempDTO);
		}
		return itemDTOs;
	}

	@Override
	public void addItems(ItemAddDTO itemAddDTO) {
		// TODO Auto-generated method stub
		Item item = mapper.map(itemAddDTO, Item.class);
		itemRepo.save(item);
	}

	@Override
	public void deleteItem(long id) {
		// TODO Auto-generated method stub
		itemRepo.deleteById(id);
	}

	@Override
	public void updateItem(long id, ItemAddDTO itemDTO) {
		// TODO Auto-generated method stub
		Item item = mapper.map(itemDTO, Item.class);
		item.setId(id);
		itemRepo.save(item);
	}
}
