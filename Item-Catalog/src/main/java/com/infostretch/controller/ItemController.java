package com.infostretch.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.infostretch.dto.ItemAddDTO;
import com.infostretch.dto.ItemDTO;
import com.infostretch.entity.Item;
import com.infostretch.services.ItemService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("v1/cart")
public class ItemController {

	private static final Logger logger = LoggerFactory.getLogger(ItemController.class);

	@Autowired
	ItemService itemService;

	@GetMapping("/items")
	@ApiOperation(value = "Get all items",
				  notes = "It returns collection of items")
	public ResponseEntity<List<ItemDTO>> getItems() {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return ResponseEntity.ok(itemService.getItems());
	}

	@PostMapping("/items")
	public ResponseEntity<Object> addItems(@RequestBody ItemAddDTO itemAddDTO) {
		itemService.addItems(itemAddDTO);
		return ResponseEntity.status(HttpStatus.CREATED).body(itemAddDTO);
	}

	@DeleteMapping("/items/{id}")
	public ResponseEntity deleteItem(@PathVariable long id) {
		itemService.deleteItem(id);
		return ResponseEntity.status(HttpStatus.ACCEPTED).build();
	}

	@PutMapping("/items/{id}")
	public ResponseEntity<ItemAddDTO> updateItem(@PathVariable long id, @RequestBody ItemAddDTO itemDTO) {
		itemService.updateItem(id, itemDTO);
		return ResponseEntity.status(HttpStatus.ACCEPTED).body(itemDTO);
	}

}
